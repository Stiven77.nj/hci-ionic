import { Component } from '@angular/core';

/* Card modal */
import { ModalController } from '@ionic/angular';
import { DetailsModalPage } from '../details-modal/details-modal.page';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  constructor(private modalController: ModalController) {}

  /* Funcion para abrir el modal */
  async openModule() {
    const modal = await this.modalController.create({
      component: DetailsModalPage
    });
  
    return await modal.present();
  }

}
