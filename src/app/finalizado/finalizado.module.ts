import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FinalizadoPageRoutingModule } from './finalizado-routing.module';

import { FinalizadoPage } from './finalizado.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FinalizadoPageRoutingModule
  ],
  declarations: [FinalizadoPage]
})
export class FinalizadoPageModule {}
