import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TranscursoPageRoutingModule } from './transcurso-routing.module';

import { TranscursoPage } from './transcurso.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranscursoPageRoutingModule
  ],
  declarations: [TranscursoPage]
})
export class TranscursoPageModule {}
