import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TranscursoPage } from './transcurso.page';

const routes: Routes = [
  {
    path: '',
    component: TranscursoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TranscursoPageRoutingModule {}
