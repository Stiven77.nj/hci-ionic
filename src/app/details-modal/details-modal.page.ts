import { Component, OnInit } from '@angular/core';

/* Card modal */
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-details-modal',
  templateUrl: './details-modal.page.html',
  styleUrls: ['./details-modal.page.scss'],
})
export class DetailsModalPage implements OnInit {

  constructor(private modalController: ModalController) { }

  ngOnInit() {
  }

  /* Funcion para cerrar el modal */
  dismiss() {
    return this.modalController.dismiss();
  }

}
